<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220523140041 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE list_viajes (id INT AUTO_INCREMENT NOT NULL, viajero_id INT NOT NULL, list_viajes_id INT NOT NULL, id_viajero INT NOT NULL, id_viajes INT NOT NULL, INDEX IDX_7617F999FAF4CFE9 (viajero_id), INDEX IDX_7617F999D22E990A (list_viajes_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE viajero (id INT AUTO_INCREMENT NOT NULL, cedula VARCHAR(255) NOT NULL, nombre VARCHAR(255) NOT NULL, fecha_nacimiento DATE NOT NULL, telefono VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE viajes (id INT AUTO_INCREMENT NOT NULL, codigo VARCHAR(255) NOT NULL, numero_plaza INT NOT NULL, lugar_origen VARCHAR(255) NOT NULL, precio DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE list_viajes ADD CONSTRAINT FK_7617F999FAF4CFE9 FOREIGN KEY (viajero_id) REFERENCES viajero (id)');
        $this->addSql('ALTER TABLE list_viajes ADD CONSTRAINT FK_7617F999D22E990A FOREIGN KEY (list_viajes_id) REFERENCES viajes (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE list_viajes DROP FOREIGN KEY FK_7617F999FAF4CFE9');
        $this->addSql('ALTER TABLE list_viajes DROP FOREIGN KEY FK_7617F999D22E990A');
        $this->addSql('DROP TABLE list_viajes');
        $this->addSql('DROP TABLE viajero');
        $this->addSql('DROP TABLE viajes');
    }
}
