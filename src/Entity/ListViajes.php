<?php

namespace App\Entity;

use App\Repository\ListViajesRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass=ListViajesRepository::class)
 * @ApiResource()
 */
class ListViajes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_viajero;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_viajes;

    /**
     * @ORM\ManyToOne(targetEntity=Viajero::class, inversedBy="listViajes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Viajero;

    /**
     * @ORM\ManyToOne(targetEntity=Viajes::class, inversedBy="listViajes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ListViajes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdViajero(): ?int
    {
        return $this->id_viajero;
    }

    public function setIdViajero(int $id_viajero): self
    {
        $this->id_viajero = $id_viajero;

        return $this;
    }

    public function getIdViajes(): ?int
    {
        return $this->id_viajes;
    }

    public function setIdViajes(int $id_viajes): self
    {
        $this->id_viajes = $id_viajes;

        return $this;
    }

    public function getViajero(): ?Viajero
    {
        return $this->Viajero;
    }

    public function setViajero(?Viajero $Viajero): self
    {
        $this->Viajero = $Viajero;

        return $this;
    }

    public function getListViajes(): ?Viajes
    {
        return $this->ListViajes;
    }

    public function setListViajes(?Viajes $ListViajes): self
    {
        $this->ListViajes = $ListViajes;

        return $this;
    }
}
