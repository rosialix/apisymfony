<?php

namespace App\Entity;

use App\Repository\ViajesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass=ViajesRepository::class)
 * @ApiResource()
 */
class Viajes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codigo;

    /**
     * @ORM\Column(type="integer")
     */
    private $numero_plaza;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $destino;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lugar_origen;

    /**
     * @ORM\Column(type="float")
     */
    private $precio;

    /**
     * @ORM\OneToMany(targetEntity=ListViajes::class, mappedBy="ListViajes")
     */
    private $listViajes;

    public function __construct()
    {
        $this->listViajes = new ArrayCollection();
    }

      public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getNumeroPlaza(): ?int
    {
        return $this->numero_plaza;
    }

    public function setNumeroPlaza(int $numero_plaza): self
    {
        $this->numero_plaza = $numero_plaza;

        return $this;
    }
    public function getDestino(): ?int
    {
        return $this->destino;
    }

    public function setDestino(int $destino): self
    {
        $this->destino = $destino;

        return $this;
    }
    
    public function getLugarOrigen(): ?string
    {
        return $this->lugar_origen;
    }

    public function setLugarOrigen(string $lugar_origen): self
    {
        $this->lugar_origen = $lugar_origen;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * @return Collection<int, ListViajes>
     */
    public function getListViajes(): Collection
    {
        return $this->listViajes;
    }

    public function addListViaje(ListViajes $listViaje): self
    {
        if (!$this->listViajes->contains($listViaje)) {
            $this->listViajes[] = $listViaje;
            $listViaje->setListViajes($this);
        }

        return $this;
    }

    public function removeListViaje(ListViajes $listViaje): self
    {
        if ($this->listViajes->removeElement($listViaje)) {
            // set the owning side to null (unless already changed)
            if ($listViaje->getListViajes() === $this) {
                $listViaje->setListViajes(null);
            }
        }

        return $this;
    }

}
