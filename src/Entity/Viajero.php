<?php

namespace App\Entity;

use App\Repository\ViajeroRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
* @ORM\Entity(repositoryClass=ViajeroRepository::class)
* @ApiResource()
*/
class Viajero
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cedula;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha_nacimiento;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telefono;

    /**
     * @ORM\OneToMany(targetEntity=ListViajes::class, mappedBy="Viajero")
     */
    private $listViajes;

    public function __construct()
    {
        $this->listViajes = new ArrayCollection();
    }

    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCedula(): ?string
    {
        return $this->cedula;
    }

    public function setCedula(string $cedula): self
    {
        $this->cedula = $cedula;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getFechaNacimiento(): ?\DateTimeInterface
    {
        return $this->fecha_nacimiento;
    }

    public function setFechaNacimiento(\DateTimeInterface $fecha_nacimiento): self
    {
        $this->fecha_nacimiento = $fecha_nacimiento;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * @return Collection<int, ListViajes>
     */
    public function getListViajes(): Collection
    {
        return $this->listViajes;
    }

    public function addListViaje(ListViajes $listViaje): self
    {
        if (!$this->listViajes->contains($listViaje)) {
            $this->listViajes[] = $listViaje;
            $listViaje->setViajero($this);
        }

        return $this;
    }

    public function removeListViaje(ListViajes $listViaje): self
    {
        if ($this->listViajes->removeElement($listViaje)) {
            // set the owning side to null (unless already changed)
            if ($listViaje->getViajero() === $this) {
                $listViaje->setViajero(null);
            }
        }

        return $this;
    }

}
