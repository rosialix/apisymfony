-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 23-05-2022 a las 22:30:03
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `turismo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20220523140041', '2022-05-23 11:11:40', 12483);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `list_viajes`
--

CREATE TABLE `list_viajes` (
  `id` int(11) NOT NULL,
  `viajero_id` int(11) NOT NULL,
  `list_viajes_id` int(11) NOT NULL,
  `id_viajero` int(11) NOT NULL,
  `id_viajes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `list_viajes`
--

INSERT INTO `list_viajes` (`id`, `viajero_id`, `list_viajes_id`, `id_viajero`, `id_viajes`) VALUES
(6, 1, 1, 1, 1),
(7, 1, 2, 1, 2),
(8, 2, 1, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_viajes`
--

CREATE TABLE `registro_viajes` (
  `id` int(11) NOT NULL,
  `viajero_id` int(11) NOT NULL,
  `viajes_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `registro_viajes`
--

INSERT INTO `registro_viajes` (`id`, `viajero_id`, `viajes_id`) VALUES
(1, 1, 2),
(2, 1, 2),
(3, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `viajero`
--

CREATE TABLE `viajero` (
  `id` int(11) NOT NULL,
  `cedula` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registroviajes_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `viajero`
--

INSERT INTO `viajero` (`id`, `cedula`, `nombre`, `fecha_nacimiento`, `telefono`, `registroviajes_id`) VALUES
(1, '11234432', 'Jose Vasquez', '2017-05-17', '0412-2347898', 0),
(2, '15234890', 'Belen Cardenas', '2012-05-27', '0426-4321987', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `viajes`
--

CREATE TABLE `viajes` (
  `id` int(11) NOT NULL,
  `codigo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero_plaza` int(11) NOT NULL,
  `lugar_origen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio` double NOT NULL,
  `destino` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `viajes`
--

INSERT INTO `viajes` (`id`, `codigo`, `numero_plaza`, `lugar_origen`, `precio`, `destino`) VALUES
(1, '1234', 5, 'Aragua', 150.12, 'Margarita'),
(2, '1235', 5, 'Zulia', 200.98, 'Gran Sabana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `viajes_registro_viajes`
--

CREATE TABLE `viajes_registro_viajes` (
  `viajes_id` int(11) NOT NULL,
  `registro_viajes_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `list_viajes`
--
ALTER TABLE `list_viajes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7617F999FAF4CFE9` (`viajero_id`),
  ADD KEY `IDX_7617F999D22E990A` (`list_viajes_id`);

--
-- Indices de la tabla `registro_viajes`
--
ALTER TABLE `registro_viajes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7368481FFAF4CFE9` (`viajero_id`),
  ADD KEY `IDX_7368481F9FE29561` (`viajes_id`);

--
-- Indices de la tabla `viajero`
--
ALTER TABLE `viajero`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `viajes`
--
ALTER TABLE `viajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `viajes_registro_viajes`
--
ALTER TABLE `viajes_registro_viajes`
  ADD PRIMARY KEY (`viajes_id`,`registro_viajes_id`),
  ADD KEY `IDX_621260119FE29561` (`viajes_id`),
  ADD KEY `IDX_62126011AF20D69` (`registro_viajes_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `list_viajes`
--
ALTER TABLE `list_viajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `registro_viajes`
--
ALTER TABLE `registro_viajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `viajero`
--
ALTER TABLE `viajero`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `viajes`
--
ALTER TABLE `viajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `list_viajes`
--
ALTER TABLE `list_viajes`
  ADD CONSTRAINT `FK_7617F999D22E990A` FOREIGN KEY (`list_viajes_id`) REFERENCES `viajes` (`id`),
  ADD CONSTRAINT `FK_7617F999FAF4CFE9` FOREIGN KEY (`viajero_id`) REFERENCES `viajero` (`id`);

--
-- Filtros para la tabla `registro_viajes`
--
ALTER TABLE `registro_viajes`
  ADD CONSTRAINT `FK_7368481F9FE29561` FOREIGN KEY (`viajes_id`) REFERENCES `viajes` (`id`),
  ADD CONSTRAINT `FK_7368481FFAF4CFE9` FOREIGN KEY (`viajero_id`) REFERENCES `viajero` (`id`);

--
-- Filtros para la tabla `viajes_registro_viajes`
--
ALTER TABLE `viajes_registro_viajes`
  ADD CONSTRAINT `FK_621260119FE29561` FOREIGN KEY (`viajes_id`) REFERENCES `viajes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_62126011AF20D69` FOREIGN KEY (`registro_viajes_id`) REFERENCES `registro_viajes` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
